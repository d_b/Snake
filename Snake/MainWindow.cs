﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;
using System.Windows.Input;

namespace Snake
{
    public partial class MainWindow : Form
    {
        private SnakeGame _game;
        private bool _allowCheating = false;

        public MainWindow()
        {
            InitializeComponent();
            KeyPreview = true;

            _game = new SnakeGame(GamePanel, Score_Label, State_Label, GameTimer);
            _game.SnakeDied += OnSnakeDied;
            GameTimer.Interval = 75;
            GameTimer.Tick += _game.Update;
        }

        private void OnSnakeDied()
        {
            StopGame();
            State_Label.Text = "State: Dead";
        }

        private void StartGame()
        {
            _game.PrepareStart();
            GameTimer.Enabled = true;
            GamePanel.Paint += _game.PaintEventHandler;
            State_Label.Text = "State: Running";
            Score_Label.Text = "Score: " + _game.Score;
            // Trace.WriteLine("Game and timer started");
        }

        private void StopGame()
        {
            GameTimer.Enabled = false;
            _game.Reset();
            GamePanel.Paint -= _game.PaintEventHandler;
            _allowCheating = false;
            // Trace.WriteLine("Game and timer stopped");
        }

        private void PauseGame()
        {
            GameTimer.Enabled = false;
            GamePanel.Paint -= _game.PaintEventHandler;
            State_Label.Text = "State: Paused";
            // Trace.WriteLine("Game and timer paused");
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            GamePad.UpdateKey(e.KeyCode, true);
            //Trace.WriteLine(e.KeyCode + " true");

            if (e.KeyCode == Keys.Enter)
            {
                if (!GameTimer.Enabled)
                {
                    StartGame();
                    if (e.Shift)
                    {
                        _allowCheating = true;
                    }
                }
                else if (GameTimer.Enabled)
                {
                    PauseGame();
                }
            }
            else if (e.KeyCode == Keys.Space && _allowCheating)
            {
                _game.GrowSnakeCheat();
            }
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            GamePad.UpdateKey(e.KeyCode, false);
            //Trace.WriteLine(e.KeyCode + " false");
        }
    }
}
