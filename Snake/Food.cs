﻿using System;
using System.Drawing;

namespace Snake
{
    public class Food
    {
        private const int Width = 20;
        private const int Height = 20;
        public static Rectangle Body = new Rectangle(0, 0, Width, Height);

        public static void PutAtNewPosition(int maxX, int maxY)
        {
            Random random = new Random();
            int rasterizedX = maxX / Width;
            int rasterizedY = maxY / Height;

            Body.X = random.Next(0, rasterizedX - 1) * Width;
            Body.Y = random.Next(0, rasterizedY - 1) * Height;
        }

        public static void Draw(Graphics g)
        {
            using (Brush brush = new SolidBrush(Color.DarkRed))
            {
                g.FillRectangle(brush, Body);
            }
        }
    }
}