﻿using System.Windows.Forms;

namespace Snake
{
    static class MainProgram
    {
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}
