﻿using System.Collections;
using System.Windows.Forms;

namespace Snake
{
    public class GamePad
    {
        private static Hashtable _keys = new Hashtable();

        public static void UpdateKey(Keys key, bool state)
        {
            _keys[key] = state;
        }

        public static bool IsKeyPressed(Keys key)
        {
            if (_keys[key] == null)
            {
                return false;
            }
            else
            {
                return (bool)_keys[key];
            }
        }
    }
}