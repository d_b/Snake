﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using System.Security.Policy;
using System.Windows.Forms;

namespace Snake
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    public class Snake
    {
        public Direction Direction { get; set; }

        private const int _cellWidth = 20; // in pixels
        private const int _cellHeight = 20;
        private Position2D _startingPosition = new Position2D(_cellWidth * 3, _cellHeight * 1);
        private const int _speed = _cellWidth; // in pixels per update
        private LinkedList<Rectangle> _body = new LinkedList<Rectangle>(); 

        public Snake()
        {
            Reset();
        }

        public void Reset()
        {
            _body.Clear();
            // adds the head at startingposition
            _body.AddLast(new Rectangle(_startingPosition.X, _startingPosition.Y, _cellWidth, _cellHeight));
            Direction = Direction.Down;
        }

        public void Draw(Graphics g)
        {
            int i = 0;
            using (Brush brush = new SolidBrush(Color.DarkGreen))
            using (Brush headBrush = new SolidBrush(Color.GreenYellow))
            {
                foreach (Rectangle bodyPart in _body)
                {
                    if (i == 0)
                    {
                        g.FillRectangle(headBrush, bodyPart);
                        i++;
                    }
                    else
                    {
                        g.FillRectangle(brush, bodyPart);
                    }
                }
            }
        }

        public void Update()
        {
            // create Rectangle at new position and add it at beginning
            int newX = _body.First.Value.X;
            int newY = _body.First.Value.Y;
            switch (Direction)
            {
                case Direction.Left:
                    newX -= _speed;
                    break;
                case Direction.Right:
                    newX += _speed;
                    break;
                case Direction.Up:
                    newY -= _speed;
                    break;
                case Direction.Down:
                    newY += _speed;
                    break;
            }
            Rectangle newHead = new Rectangle(newX, newY, _cellWidth, _cellHeight);
            _body.AddFirst(newHead);

            // remove rectangle at end
            _body.RemoveLast();
        }

        public void Grow()
        {
            // add body part at end
            int newX = _body.Last.Value.X;
            int newY = _body.Last.Value.Y;
            Rectangle newTail = new Rectangle(newX, newY, _cellWidth, _cellHeight);
            _body.AddLast(newTail);
        }

        public bool CollidesWithItself()
        {
            bool collidesWithItself = false;
            int i = 0;
            foreach (Rectangle bodyPart in _body)
            {
                // don't test head against head
                if (i == 0)
                {
                    i++;
                    continue;
                }

                if (_body.First.Value.IntersectsWith(bodyPart))
                {
                    collidesWithItself = true;
                    break;
                }
            }
            return collidesWithItself;
        }

        public bool CollidesWithWorldBoundaries(int width, int height)
        {
            bool collidesWithWorldBoundaries = false;
            Rectangle head = _body.First.Value;

            // left border
            if (head.X < 0)
            {
                collidesWithWorldBoundaries = true;
            }
            // right border
            else if ((head.X + _cellWidth) > width)
            {
                collidesWithWorldBoundaries = true;
            }
            // top border
            else if (head.Y < 0)
            {
                collidesWithWorldBoundaries = true;
            }
            // bottom border
            else if ((head.Y + _cellHeight) > height)
            {
                collidesWithWorldBoundaries = true;
            }

            return collidesWithWorldBoundaries;
        }

        public bool CollidesWithRect(Rectangle other)
        {
            return _body.First.Value.IntersectsWith(other);
        }
    }
}