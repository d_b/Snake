﻿namespace Snake
{
    public class Position2D
    {
        public int X = 0;
        public int Y = 0;

        public Position2D(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}