﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace Snake
{
    public class SnakeGame
    {
        public delegate void SnakeDiedEventHandler();
        public event SnakeDiedEventHandler SnakeDied;

        private Panel _gamePanel;
        private Snake _snake = new Snake();
        public int Score { get; private set; }
        private Label _scoreLabel;
        private Label _stateLabel;
        private Timer _gameTimer;
        private bool _isDead = false;

        public SnakeGame(Panel gamePanel, Label scoreLabel, Label stateLabel, Timer gameTimer)
        {
            Score = 0;
            _gamePanel = gamePanel;
            _scoreLabel = scoreLabel;
            _stateLabel = stateLabel;
            _gameTimer = gameTimer;
            Food.PutAtNewPosition(_gamePanel.Width, _gamePanel.Height);
        }

        public void Reset()
        {
            _snake.Reset();
            Food.PutAtNewPosition(_gamePanel.Width, _gamePanel.Height);
            Score = 0;
        }

        public void PrepareStart()
        {
            _isDead = false;
        }

        public void Update(object sender, EventArgs e)
        {
            HandleInput();
            _snake.Update();
            HandleCollisions();

            // make sure the panel is redrawn only
            // when snake is still alive
            if (!_isDead)
            {
                _gamePanel.Invalidate();
            }
        }

        private void HandleInput()
        {
            if (GamePad.IsKeyPressed(Keys.Up) && _snake.Direction != Direction.Down)
            {
                // Trace.WriteLine("up arrow key pressed");
                _snake.Direction = Direction.Up;
            }
            if (GamePad.IsKeyPressed(Keys.Down) && _snake.Direction != Direction.Up)
            {
                // Trace.WriteLine("down arrow key pressed");
                _snake.Direction = Direction.Down;
            }
            if (GamePad.IsKeyPressed(Keys.Left) && _snake.Direction != Direction.Right)
            {
                // Trace.WriteLine("left arrow key pressed");
                _snake.Direction = Direction.Left;
            }
            if (GamePad.IsKeyPressed(Keys.Right) && _snake.Direction != Direction.Left)
            {
                // Trace.WriteLine("right arrow key pressed");
                _snake.Direction = Direction.Right;
            }
        }

        private void HandleCollisions()
        {
            if (_snake.CollidesWithWorldBoundaries(_gamePanel.Width, _gamePanel.Height)
                || _snake.CollidesWithItself())
            {
                if (SnakeDied != null)
                {
                    SnakeDied.Invoke();
                }
                _isDead = true;
                // nothing else to do when snake is dead
                return;
            }

            if (_snake.CollidesWithRect(Food.Body))
            {
                _snake.Grow();
                Food.PutAtNewPosition(_gamePanel.Width, _gamePanel.Height);
                Score += 10;
                _scoreLabel.Text = "Score: " + Score;
            }
        }

        public void PaintEventHandler(object sender, PaintEventArgs e)
        {
            _snake.Draw(e.Graphics);
            Food.Draw(e.Graphics);
        }

        public void GrowSnakeCheat()
        {
            _snake.Grow();
        }
    }
}